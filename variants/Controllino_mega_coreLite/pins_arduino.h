/*
  pins_arduino.h - Pin definition functions for Arduino

*/

#include "../mega/pins_arduino.h"
#include "pins.h"

#define CONTROLLINO_INIT    MEGA_INIT_RELE      \
                            MEGA_INIT_DO        \
                            MAXI_INIT_OVL       \
                            MEGA_INIT_DI        \
                            MAXI_INIT_SPI_SS    \
                            MAXI_INIT_RS485

#undef NUM_DIGITAL_PINS 
#define NUM_DIGITAL_PINS            70
#undef NUM_ANALOG_INPUTS
#define NUM_ANALOG_INPUTS           16
#define CONTROLLINO_MEGA
