#ifndef PINS_INIT_MEGA_H
#define PINS_INIT_MEGA_H

#ifdef __cplusplus
extern "C" {
#endif

#include "../Controllino_maxi_coreLite/pins.h"

#define MEGA_INIT_RELE      MAXI_INIT_RELE      \
                            PORTC &= ~0x3F;     \
                            DDRC |= 0x3F;

#define Rele10_On()    SBI(PORTC,PC5)
#define Rele10_Off()   CBI(PORTC,PC5)
#define Rele11_On()    SBI(PORTC,PC4)
#define Rele11_Off()   CBI(PORTC,PC4)
#define Rele12_On()    SBI(PORTC,PC3)
#define Rele12_Off()   CBI(PORTC,PC3)
#define Rele13_On()    SBI(PORTC,PC2)
#define Rele13_Off()   CBI(PORTC,PC2)
#define Rele14_On()    SBI(PORTC,PC1)
#define Rele14_Off()   CBI(PORTC,PC1)
#define Rele15_On()    SBI(PORTC,PC0)
#define Rele15_Off()   CBI(PORTC,PC0)

#define MEGA_INIT_DO        MAXI_INIT_DO         \
                            PORTL = 0x00;        \
                            DDRL |= 0xFF;        \
                            PORTD &= ~0x70;      \
                            DDRD |= 0x70;        \
                            PORTJ &= ~(1 << PJ4);\
                            DDRJ |= (1 << PJ4);

#define D12_On()      SBIE(PORTL,PL7)
#define D12_Off()     CBIE(PORTL,PL7)
#define D13_On()      SBIE(PORTL,PL6)
#define D13_Off()     CBIE(PORTL,PL6)
#define D14_On()      SBIE(PORTL,PL5)
#define D14_Off()     CBIE(PORTL,PL5)
#define D15_On()      SBIE(PORTL,PL4)
#define D15_Off()     CBIE(PORTL,PL4)
#define D16_On()      SBIE(PORTL,PL3)
#define D16_Off()     CBIE(PORTL,PL3)
#define D17_On()      SBIE(PORTL,PL2)
#define D17_Off()     CBIE(PORTL,PL2)
#define D18_On()      SBIE(PORTL,PL1)
#define D18_Off()     CBIE(PORTL,PL1)
#define D19_On()      SBIE(PORTL,PL0)
#define D19_Off()     CBIE(PORTL,PL0)
#define D20_On()      SBI(PORTD,PD4)
#define D20_Off()     CBI(PORTD,PD4)
#define D21_On()      SBI(PORTD,PD5)
#define D21_Off()     CBI(PORTD,PD5)
#define D22_On()      SBI(PORTD,PD6)
#define D22_Off()     CBI(PORTD,PD6)
#define D23_On()      SBIE(PORTJ,PJ4)
#define D23_Off()     CBIE(PORTJ,PJ4)

#define MEGA_INIT_DI  MAXI_INIT_DI              \
                      DDRK = 0x00;              \
                      PORTK = 0x00;             \
                      DDRD &= ~0x80;            \
                      PORTD &= ~0x80;           \
                      DDRG &= ~(1 << PG1);      \
                      PORTG &= ~(1 << PG1);

#define A10_In()        (PINK & (1 << PK2))
#define A11_In()        (PINK & (1 << PK3))
#define A12_In()        (PINK & (1 << PK4))
#define A13_In()        (PINK & (1 << PK5))
#define A14_In()        (PINK & (1 << PK6))
#define A15_In()        (PINK & (1 << PK7))
#define I16_In()        (PIND & (1 << PD7))
#define I17_In()        (PING & (1 << PG2))
#define I18_In()        (PING & (1 << PG1))

#ifdef __cplusplus
}
#endif

#endif /* PINS_INIT_MEGA_H */
